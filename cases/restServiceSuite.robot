*** Settings ***
Library    Collections
Library    RequestsLibrary 
*** Test Cases ***
testCase1
    Create Session    google    https://www.google.com  
    ${resp}=    Get Request    google   /
    Should Be Equal As Strings    ${resp.status_code}   200    

testCase2
    Create Session    alias    https://jsonplaceholder.typicode.com  
    ${resp}=    Get Request    alias   /users/1
    Should Be Equal As Strings    ${resp.status_code}   200 
    log     ${resp.json()}
    Dictionary Should Contain Value    ${resp.json()}    Bret	    
         
testProxy
    ${proxy}    Create Dictionary    http=http://acme.com:912    https=http://acme.com:913
    Create Session    github    https://api.github.com    proxies=${proxy}
    ${resp}    Get Request    github    /
    Should Be Equal As Strings    ${resp.status_code}    200     
    
testCookies
    ${cookies}=    Create Dictionary	    userid=1234567	    last_visit=2017-12-22
    Create Session	    github	    http://api.github.com	    cookies=${cookies}
    ${resp}=    Get Request	    github	    /
    Should Be Equal As Strings	    ${resp.status_code}	    200                            