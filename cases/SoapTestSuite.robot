*** Settings ***
Library    SudsLibrary  
Library    Collections    
Library    BuiltIn    
  
*** Test Cases ***
TestCase1  
    Create Soap Client    http://ws.cdyne.com/ip2geo/ip2geo.asmx?WSDL  		
	${args} =  Create Dictionary      ipAddress=150.162.2.1     licenseKey=null
	${result}=	Call Soap Method	ResolveIP	${args}
	${country}           Get Wsdl Object Attribute        ${result}   Country    
    Should Be Equal As Strings    ${country}     Brazil     