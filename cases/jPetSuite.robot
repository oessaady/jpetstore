*** Settings ***
Resource  ../resource/keywords/keywords.txt
Suite Setup    open my browser
Suite Teardown     close my browser

*** Test Cases ***
JpetStoreCatalog
    login on the store
    select categorie
    select sub categorie
    add product to card
    procceed to checkout
    validate form
    confirm and submit
    check validation message